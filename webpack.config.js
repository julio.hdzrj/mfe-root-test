const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (webpackConfigEnv, argv) => {
  const orgName = "mfetest";
  const defaultConfig = singleSpaDefaults({
    orgName,
    projectName: "root-config",
    webpackConfigEnv,
    argv,
    disableHtmlGeneration: true,
  });

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    plugins: [
      new HtmlWebpackPlugin({
        inject: false,
        template: "src/index.ejs",
        templateParameters: {
          isLocal: webpackConfigEnv && webpackConfigEnv.isLocal,
          orgName,
        },
      }),
    ],
    devServer: {
      port: 5000,
      historyApiFallback: true,
      open: true,
      proxy: [
        {
          context: ["/user/**"],
          secure: false, // had an expression which was resolving to true
          changeOrigin: true,
          target: 'https://mfe-root-test.vercel.app', // http://localhost:5000/ check in production change url
          router: () => 'https://apimocha.com/mfeapi',
          logLevel: 'debug' /*optional*/,
        }
      ],
    },
  });
};
